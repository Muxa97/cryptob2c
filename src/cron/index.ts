import axios from 'axios';
import {
  BLOCK_TO_START_RECEIVING,
  CRON_TIMEOUT,
  ETHERSCAN_API_KEY,
} from 'src/config';
import Transaction from '../connection/models/transaction';

const loadPreviousData = async () => {
  const firstTx = await Transaction.findOne({
    order: [['blockNumber', 'ASC']],
    limit: 1,
  });

  const firstLoadedBlock = firstTx.blockNumber;
  const loadFrom = BLOCK_TO_START_RECEIVING;

  const pages = Math.ceil((firstLoadedBlock - loadFrom) / 50);
  const pgArray = new Array(pages);

  for (const page of pgArray) {
    const result = await axios.get(
      `https://api.etherscan.io/api?module=account&action=txlistinternal&startblock=${BLOCK_TO_START_RECEIVING}&endblock=${firstLoadedBlock}&page=${page}&offset=100&sort=asc&apikey=${ETHERSCAN_API_KEY}`,
    );
    const txs = result.data.result;

    await Transaction.bulkCreate(
      txs.map((tx) => {
        return {
          txHash: tx.hash,
          blockNumber: +tx.blockNumber,
          sender: tx.from,
          receiver: tx.to,
          value: (1 / 10 ** 18) * Number.parseInt(tx.value.substring(2), 16),
        };
      }),
      {
        updateOnDuplicate: ['blockNumber', 'receiver'],
      },
    );
  }
};

const start = async () => {
  setInterval(async () => {
    const blockNumber: any = await axios.get(
      'https://api.etherscan.io/api?module=proxy&action=eth_blockNumber',
    );
    const response: any = await axios.get(
      `https://api.etherscan.io/api?module=proxy&action=eth_getBlockByNumber&tag=${blockNumber.data.result}&boolean=true&apikey=${ETHERSCAN_API_KEY}`,
    );

    const transactions = response?.data?.result?.transactions;

    try {
      if (transactions) {
        await Transaction.bulkCreate(
          transactions.map((tx) => {
            return {
              txHash: tx.hash,
              blockNumber: +tx.blockNumber,
              sender: tx.from,
              receiver: tx.to,
              value:
                (1 / 10 ** 18) * Number.parseInt(tx.value.substring(2), 16),
            };
          }),
          {
            updateOnDuplicate: ['blockNumber', 'receiver'],
          },
        );
      }
    } catch (err) {
      console.log(err);
    }
  }, CRON_TIMEOUT);
};

export { start, loadPreviousData };
