'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('transactions', {
      txHash: {
        type: Sequelize.STRING,
        primaryKey: true,
      },
      blockNumber: {
        type: Sequelize.INTEGER,
        index: true,
        allowNull: false,
      },
      sender: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      receiver: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      value: {
        type: Sequelize.DOUBLE,
        allowNull: false,
      },
    });
  },

  async down(queryInterface) {
    await queryInterface.dropTable('transactions');
    return;
  },
};
