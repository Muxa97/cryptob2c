import {
  Model,
  InferAttributes,
  InferCreationAttributes,
  DataTypes,
} from 'sequelize';
import sequelize from '../index';

class Transaction extends Model<
  InferAttributes<Transaction>,
  InferCreationAttributes<Transaction>
> {
  declare txHash: string;
  declare blockNumber: number;
  declare sender: string;
  declare receiver: string;
  declare value: number;
}

Transaction.init(
  {
    txHash: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    blockNumber: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    sender: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    receiver: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    value: {
      type: DataTypes.DOUBLE,
      allowNull: false,
    },
  },
  {
    tableName: 'transactions',
    timestamps: null,
    sequelize,
  },
);

export default Transaction;
