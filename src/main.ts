import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { APP_PORT } from './config';
import sequelize from './connection';
import { start, loadPreviousData } from './cron/index';

async function bootstrap() {
  try {
    await sequelize.authenticate();

    const app = await NestFactory.create(AppModule);
    await app.listen(APP_PORT);
    await loadPreviousData();
    await start();
  } catch (err) {
    console.log(err);
  }
}
bootstrap();
