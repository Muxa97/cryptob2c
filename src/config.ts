import { load } from 'ts-dotenv';
const env = load({
  ETHERSCAN_API_KEY: String,
  APP_PORT: Number,
  CRON_TIMEOUT: Number,
  LAST_BLOCKS_NUMBER: Number,
  BLOCK_TO_START_RECEIVING: Number,
  DB_NAME: String,
  DB_USER: String,
  DB_PASS: String,
  DB_HOST: String,
});

const ETHERSCAN_API_KEY = env.ETHERSCAN_API_KEY;
const APP_PORT = env.APP_PORT || 3000;
const CRON_TIMEOUT = env.CRON_TIMEOUT || 10000;
const LAST_BLOCKS_NUMBER = env.LAST_BLOCKS_NUMBER || 100;
const BLOCK_TO_START_RECEIVING = env.BLOCK_TO_START_RECEIVING || 9842805;
const DB_NAME = env.DB_NAME || 'cryptob2c';
const DB_HOST = env.DB_HOST || 'localhost';
const DB_USER = env.DB_USER || 'postgres';
const DB_PASS = env.DB_PASS || 'postgres';

export {
  ETHERSCAN_API_KEY,
  APP_PORT,
  CRON_TIMEOUT,
  LAST_BLOCKS_NUMBER,
  BLOCK_TO_START_RECEIVING,
  DB_NAME,
  DB_HOST,
  DB_USER,
  DB_PASS,
};
