import { Injectable } from '@nestjs/common';
import { Op } from 'sequelize';
import { LAST_BLOCKS_NUMBER } from './config';
import Transaction from './connection/models/transaction';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getBalance(): Promise<string> {
    const lastTx = await Transaction.findOne({
      order: [['blockNumber', 'DESC']],
      limit: 1,
    });

    if (!lastTx) return '';

    const blockNumber = lastTx.blockNumber;

    const txs = await Transaction.findAll({
      where: {
        [Op.and]: [
          {
            blockNumber: {
              [Op.gte]: blockNumber - LAST_BLOCKS_NUMBER,
            },
          },
          {
            blockNumber: {
              [Op.lte]: blockNumber,
            },
          },
        ],
      },
    });

    const balances: any = txs.reduce((acc: any, tx: Transaction) => {
      if (tx.receiver in acc) {
        acc[tx.receiver] += tx.value;
      } else {
        acc[tx.receiver] = tx.value;
      }

      if (tx.sender in acc) {
        acc[tx.sender] -= tx.value;
      } else {
        acc[tx.sender] = -tx.value;
      }

      return acc;
    }, {});

    const addresses: string[] = Object.keys(balances);
    let addrIndex = 0;

    Object.values(balances).reduce(
      (maxBalance: number, balance: number, index: number) => {
        if (Math.abs(balance) > maxBalance) {
          maxBalance = Math.abs(balance);
          addrIndex = index;
        }

        return maxBalance;
      },
      0,
    );

    return addresses[addrIndex];
  }
}
