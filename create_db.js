const { Client } = require('pg')
const { config } = require('dotenv')
config()

const pgClient = new Client({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: 'postgres',
    password: process.env.DB_PASS
  })
console.log(process.env.DB_NAME)
  pgClient.connect()
  pgClient.query(
      `SELECT 'CREATE DATABASE ${process.env.DB_NAME}' 
        WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '${process.env.DB_NAME}');`
    ).then((result) => {
        const columnName = result.fields[0].name
        row = result.rows[0]
        return pgClient.query(row[columnName] + ';')
    })
    .then(() => pgClient.end())
    .catch(err => console.error(err))