## Test project for Crypto B2C

### Requirements

- Node.js
- NPM
- PostgresQL

### Setup the project

1. Run the npm command `npm install` to install dependencies

2. Rename `.env.sample` to `.env` and fill all fields (required)

3. Edit `./config/config.json` for Postgres access

4. To automatically create database specified in config.json run `node create_db.js`

5. Run `npx sequelize db:migrate`

6. To recreate DB use `npx sequelize db:migrate:undo` and then use step 5.

### Start the project

Dev start in watch mode: `npm run start:dev` or without watch `npm run start`

Start prod: `npm run build` and than `npm run start:prod`